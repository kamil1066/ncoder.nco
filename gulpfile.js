const elixir = require('laravel-elixir');

elixir.config.sourcemaps = false;

const gulp = require('gulp');

elixir(function(mix) {

    //compile sass to css
    mix.sass('resources/assets/sass/app.scss', 'resources/assets/css');

    // combine css files
    mix.styles(
        [
            'css/app.css',
            'bower/vendor/slick-carousel/slick/slick.scss'
        ], 'public/css/styles.css', //output file
        'resources/assets'
    );

    const bowerPath = "bower/vendor";

    mix.scripts(
        [
            //jquery
            bowerPath + '/jquery/dist/jquery.min.js',

            //bootstrapJS
            bowerPath + '/bootstrap-sass//assets/javascripts/bootstrap.min.js',

            //slick carousel
            bowerPath + '/slick-carousel/slick/slick.min.js',

            //custom javscripts
            '/js/*.js'

        ], 'public/js/scripts.js', //output file
        'resources/assets'
    );
});