<?php

use App\RouteDispatcher;
use App\Classes\Database;
use App\Classes\ErrorHandler;

//starting session if it isn't
if(!isset($_SESSION)) session_start();
//load environmental variables
require_once __DIR__ . '/../app/config/_env.php';
//instatiate database class
new Database();
//set custom error handler
set_error_handler([new ErrorHandler(), 'handleErrors']);
//loading routes
require_once __DIR__ . '/../app/routing/routes.php';
//instatiate route dispatcher
new RouteDispatcher($router);
