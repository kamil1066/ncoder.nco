<?php

$router = new AltoRouter; //new instance of AltoRouter
$router->map('GET', '/', 'App\Controllers\IndexController^show', 'Main');
$router->map('GET', '/about' , 'App\Controllers\AboutController^show', 'About');

//admin routes
$router->map('GET', '/app_user', 'App\Controllers\Admin\DashboardController^show', 'Panel');

 