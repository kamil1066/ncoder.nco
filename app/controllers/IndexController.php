<?php

namespace App\Controllers;

use App\Classes\Mail;

class IndexController extends BaseController {

    public function show() {
        view('pages/home');

        // $mail = new Mail();
        // $data = [
        //     'to' => 'kamilzuk@zstk.edu.pl',
        //     'subject' => 'welcome to my page',
        //     'view' => 'welcome',
        //     'name' => 'Jon Doe',
        //     'body' => 'testing email template'
        // ];

        // if ($mail->send($data)) {
        //     echo "Wiadomość wysłana!";
        // } else {
        //     echo "Niestety nie udało się wysłać wiadmości!!! Spróbuj ponownie później";
        // }
    }

}
