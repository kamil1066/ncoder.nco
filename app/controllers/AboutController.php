<?php

namespace App\Controllers;

/**
 * Description of AboutController
 *
 * @author lenovo
 */
class AboutController {

    public function show() {
        view('pages/about');
    }

}
