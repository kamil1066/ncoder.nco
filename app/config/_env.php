<?php

use Dotenv\Dotenv;

define('BASE_PATH', realpath( __DIR__ . '/../../'));

require_once __DIR__ . '/../../vendor/autoload.php';

//loading env file 
$dotEnv = new Dotenv(BASE_PATH);
$dotEnv->load(); 