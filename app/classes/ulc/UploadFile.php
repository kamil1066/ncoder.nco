<?php

namespace \App\Classess\Ulc;

use App\classes\Session;

class UploadFile
{
    protected $filname;
    protected $max_file_size = 2097152;
    protected $extension;
    protected $path;

    /**
     * get the name of the file
     * @return mixed
     */
    public function getName()
    {
        return $this->filename;
    }
     
    /**
     * set the name of a file
     * @param $file
     * @param string $name
     */
    protected function setName($file, $name = "")
    {
        if($name === "")
        {
            $name = pathinfo($file, PATHINFO_FILENAME);
        }
        $name = strtolower(str_replace(['-', ' '], '-', $name));
        $hash = md5(microtime());
        $ext  = $this->fileExtension();
        $this->filename = "{$name}-{$hash}.{$ext}";
     }

     /**
      * set file extension
      * @param $file
      * @return mixed
      */
    protected function fileExtension($file)
    {
        return $this->extension = pathinfo($file, PATHINFO_EXTENSION);
    }

     /**
      * validate filesize
      * @param $file
      * @return bool
      */
    public static function fileSize($file)
    {
        $fileobj = new static;
        return $file > $fileobj->max_file_size ? true : false;
    }

    /**
     * checking if file is an image (extension)
     * @param $file
     * 
     */
    public static function isImage($file)
    {
        $fileobj = new static;
        $ext = $fileobj->fileExtension($file);
        $validExt = ['jpg','jpeg', 'png', 'bmp', 'gif'];

        if(!in_array(strtolower($ext), $validExt))
        {
            return false;
        }
        return true;
    }

    /**
     * check the files path where the file was uploaded to
     */
    public function path()
    {
        return $this->path;
    }
}