<?php

namespace App\Classes;

class CSRFToken
{
    /**
    * Create CSRFToken
    * 
    * @return mixed
    */
    
    public static function _token()
    {
        // Session::remove('token'); exit();
        if (!Session::has('token'))
        {
            $randomToken = base64_encode(openssl_random_pseudo_bytes(32));
            Session::add('token', $randomToken);
        }
        return Session::get('token');
    }

    /** verify CSRF Token
    * 
    * @param $requestToken
    * @return bool
    */
    
    
    public static function verifyCSRFToken($requestToken)
    {
        if (Session::has('token') and Session::get('token') === $requestToken)
        {
            Session::remove('token');
            return true;
        }
        return false;
    }

}