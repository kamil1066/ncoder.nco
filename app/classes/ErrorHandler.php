<?php

namespace App\Classes;

class ErrorHandler
{
    public function handleErrors($error_number, $error_message, $error_file, $error_line)
    {
        //getting: error number, line, message and file in which an error occured
        $error = "[{$error_number}] An error occured in file {$error_file} on line $error_line: $error_message";
        if (getenv('APP_ENV') === "local") {
            //generating error handling
            $whoops = new \Whoops\Run;
            $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
            $whoops->register();
            // we get a page with an error description
        } else {
            //we get mail with the error
            $data = [
                'to' => getenv("ADMIN_EMAIL"),
                'subject' => 'System Error',
                'view' => 'errors',
                'name' => 'Admin',
                'body' => $error
            ];
            //user gets the message stored in views/errors/errors.php
            ErrorHandler::emailAdmin($data)->outputFriendlyError();
        }
    }

    public function outputFriendlyError()
    {
        // cleaning buffer
        ob_end_clean();
        //loading a view
        view('errors/generic');
        exit;
    }

    public static function emailAdmin($data)
    {
        //sending mail wirh error to admin
        $mail = new Mail;
        $mail->send($data);
        return new static;
    }
}
