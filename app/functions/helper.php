<?php

use Philo\Blade\Blade; //importing Blade

function view($path, array $data = [])
{
    $view = __DIR__ . '/../../resources/views';
    $cache = __DIR__ . '/../../bootstrap/cache';
    // new instance of blade template
    $blade = new Blade($view, $cache); // view and cache folders indicated
    echo $blade->view()->make($path, $data)->render(); // rendering the view
}

function make($filename, $data)
{
    extract($data);

    //turning on output buffering
    ob_start();

    //include template $filename may be any of .php files in the dirctory
    include(__DIR__ . '/../../resources/views/emails/' . $filename . '.php');

    //get content of the file ($filename)
    $content = ob_get_contents();

    //turn off output buffering
    ob_end_clean();

    return $content; // get the content of the $filename
}