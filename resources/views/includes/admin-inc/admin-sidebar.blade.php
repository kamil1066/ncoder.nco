<div class="sidebar">
    {{--menu sidebar--}}
    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="/app_user" class="simple-text">
                {{ getenv('AUTHOR_NAME') }}
                {{ $admin }}
                <br> ADMIN PANEL
            </a>
        </div>
        {{-- nav start --}}
        <ul class="nav">
            <li class="active">
                <a href="/app_user">
                    <i class="fa fa-home admin-fonts" aria-hidden="true"></i>
                    <p>Panel Główny</p>
                </a>
            </li>
            <li>
                <a href="/app_user/users">
                    <i class="fa fa-users admin-fonts" aria-hidden="true"></i>
                    <p>Użytkownicy</p>
                </a>
            </li>
            <li>
                <a href="/app_user/product/add_product">
                    <i class="fa fa-plus admin-fonts"></i>
                    <p>Dodaj produkt</p>
                </a>
            </li>
            <li>
                <a href="/app_user/products_management">
                    <i class="fa fa-edit admin-fonts"></i>
                    <p>Zarządzaj produktami</p>
                </a>
            </li>
            <li>
                <a href="/app_user/product/categories">
                    <i class="fa fa-sitemap admin-fonts"></i>
                    <p>Kategorie</p>
                </a>
            </li>
            <li>
                <a href="/app_user/users/orders">
                    <i class="fas fa-cart-arrow-down admin-fonts"></i>
                    <p>Zamównienia</p>
                </a>
            </li>
            <li>
                <a href="/app_user/users/payments">
                    <i class="fa fa-shopping-cart admin-fonts"></i>
                    <p>Płatności</p>
                </a>
            </li>

        </ul>
    </div>
</div>
{{-- end of sidebar --}}
