<footer class="footer stopka">
    <div class="container-fluid">
        <nav class="pull-left">
            <ul>
                <li>
                    <a href="#">
                        Home
                    </a>
                </li>
                <li>
                    <a href="#">
                        Company
                    </a>
                </li>
                <li>
                    <a href="#">
                        Portfolio
                    </a>
                </li>
                <li>
                    <a href="#">
                        Blog
                    </a>
                </li>
            </ul>
        </nav>
        <p class="copyright pull-right" id="copyrights">
            <script type="text/javascript">
                var date = new Date().getFullYear();
                const info = 'In code we trust';
                const copy =
                    '<a href="/"><span class="brand-name"> <img class="footer-logo" src="https://ncoder.pl/wp-content/themes/twentysixteen/img/logo.png"> &copy;</span></a>';
                document.getElementById('copyrights').innerHTML = copy + "<span class=\"date\"> " + info +
                    " " + date + "</span>";
            </script>

        </p>
    </div>
</footer>
