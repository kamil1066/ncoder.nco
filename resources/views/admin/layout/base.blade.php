<!doctype html>
<html lang="pl">
	<head>
		<meta charset="utf-8" />
		<link rel="icon" type="image/png" href="/img/favicon.ico">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /> //
		<title>{{ getenv('SITE_NAME') }} - Panel Administracyjny</title>

		<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
		<meta name="viewport" content="width=device-width" />

		<!-- Bootstrap core CSS     -->
		<link href="/css/styles.css" rel="stylesheet" />

		<!--     Fonts and icons     -->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
		{{-- <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">  --}}
		{{--
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'> --}} {{--
		<link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" /> --}}
	</head>
<body>
@yield('content')

<!--   Core JS Files   -->
<script src="/js/scripts.js" type="text/javascript"></script>
{{--
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script> --}}

<!--  Charts Plugin -->
<script src="/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="/js/bootstrap-notify.js"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="/js/light-bootstrap-dashboard.js?v=1.4.0"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="/js/demo.js"></script>
<script type="text/javascript">

</script>
</body>


</html>
